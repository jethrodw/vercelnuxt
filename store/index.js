export const state = () => ({
  authenticated: false,
})

export const getters = {
  isAuthenticated: state => state.authenticated,
}

export const mutations = {
  SET_authenticated: (state, value) => state.authenticated = value,
}

export const actions = {
  async SignIn({commit}) {
      commit('SET_authenticated', true)
      this.$router.push('/two-option.vue')
      return true
  },
  
}